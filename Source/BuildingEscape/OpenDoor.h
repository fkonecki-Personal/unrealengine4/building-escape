// Fran Konecki 2021

#pragma once

#include "CoreMinimal.h"
#include "Components/AudioComponent.h"
#include "Components/ActorComponent.h"
#include <Engine/TriggerVolume.h>
#include "OpenDoor.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UOpenDoor : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOpenDoor();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void OpenCloseDoor(float DeltaTime, float FinalYaw);
	virtual float GetTotalMassOfOverlapingActors() const;
	virtual void FindAudioComponent();

private:
	UPROPERTY(EditAnywhere)
	float DoorDelay = 2.f;

	UPROPERTY(EditAnywhere)
	float TargetYawRotation = 90.0f;

	UPROPERTY(EditAnywhere)
	ATriggerVolume* PressurePlate;

	UPROPERTY(EditAnywhere)
	AActor* DoorOpeningActor;

	UPROPERTY()
	UAudioComponent* AudioComponent;

	float DelayCounter = 0.f;

	float ClosedYaw= 0.f;
	float CurrentYaw = 0.f;
	float OpenedYaw = 0.f;

	bool Opening = false;
	bool Closing = false;

	

};
