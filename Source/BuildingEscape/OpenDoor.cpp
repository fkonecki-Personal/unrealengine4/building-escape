// Fran Konecki 2021


#include "OpenDoor.h"
#include <Components/AudioComponent.h>
#include <GameFramework/Actor.h>

// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

}

// Called when the game starts
void UOpenDoor::BeginPlay() {
	Super::BeginPlay();

	if (!PressurePlate) {
		UE_LOG(LogTemp, Warning, TEXT("%s has OpenDoor component attached but no PressurePlate assigned!"),
			*GetOwner()->GetName());
	}

	ClosedYaw = GetOwner()->GetActorRotation().Yaw;
	CurrentYaw = ClosedYaw;
	OpenedYaw= ClosedYaw + TargetYawRotation;
}

// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) {
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (PressurePlate->IsOverlappingActor(DoorOpeningActor)) {

		UE_LOG(LogTemp, Warning, TEXT("Overlapping"));

		Opening = CurrentYaw != OpenedYaw;

		if (Opening && Closing) {
			Closing = false;
			DelayCounter = 0.f;
		}

		OpenCloseDoor(DeltaTime, OpenedYaw);

	} else {
		Closing = CurrentYaw != ClosedYaw;

		if (Opening && Closing) {
			Opening = false;
			DelayCounter = DoorDelay;
		}

		OpenCloseDoor(DeltaTime, ClosedYaw);
	}
}

void UOpenDoor::OpenCloseDoor(float DeltaTime, float FinalYaw) {
	DelayCounter -= DeltaTime;

	if (DelayCounter <= 0.f) {
		FRotator DoorRotation = GetOwner()->GetActorRotation();

		CurrentYaw = FMath::FInterpTo(CurrentYaw, FinalYaw, DeltaTime, 3);
		DoorRotation.Yaw = CurrentYaw;

		GetOwner()->SetActorRotation(DoorRotation);

		//AudioComponent->Play();
	}
}

float UOpenDoor::GetTotalMassOfOverlapingActors() const {
	float TotalMass = 0.f;
	TArray<AActor*> OverlapingActors;

	PressurePlate->GetOverlappingActors(OverlapingActors);

	for (AActor* Actor: OverlapingActors) {
		UE_LOG(LogTemp, Warning, TEXT("Overlaping actor %s"), *Actor->GetFName().ToString());

		TotalMass += Actor->FindComponentByClass<UPrimitiveComponent>()->GetMass();
	}

	UE_LOG(LogTemp, Warning, TEXT("Total mass %f"), TotalMass);

	return TotalMass;
}

void UOpenDoor::FindAudioComponent() {
	AudioComponent = GetOwner()->FindComponentByClass<UAudioComponent>();
}




