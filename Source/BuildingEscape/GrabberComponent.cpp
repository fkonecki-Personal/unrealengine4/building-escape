// Fran Konecki 2021

#define OUT

#include "GrabberComponent.h"
#include "GameFramework/PlayerController.h"
#include "DrawDebugHelpers.h"
#include "Components/InputComponent.h"

// Sets default values for this component's properties
UGrabberComponent::UGrabberComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UGrabberComponent::BeginPlay() {

	Super::BeginPlay();

	FindPhysicsHandle();
	SetupInputComponent();
}

void UGrabberComponent::Grab() {
	HitResult = GetFirstPhysicsBody();

	if (HitResult.GetActor()) {
		PhysicsHandle->GrabComponentAtLocation(
			HitResult.GetComponent(),
			NAME_None, 
			GetLineTraceEnd());

		UMaterial* material = HitResult.GetComponent()->GetMaterial(0)->GetMaterial();
		material->SetScalarParameterValueEditorOnly("Blend", 1);
	}
}

void UGrabberComponent::Release() {
	if (HitResult.GetActor()) {
		UMaterial* material = HitResult.GetComponent()->GetMaterial(0)->GetMaterial();
		material->SetScalarParameterValueEditorOnly("Blend", 0);

		HitResult = FHitResult::FHitResult();
	}

	PhysicsHandle->ReleaseComponent();
}

// Called every frame
void UGrabberComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) {
	
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (PhysicsHandle->GrabbedComponent)
		PhysicsHandle->SetTargetLocation(GetLineTraceEnd());
}

void UGrabberComponent::FindPhysicsHandle() {
	PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();

	if (!PhysicsHandle) 
		UE_LOG(LogTemp, Error, TEXT("No PhysicsHandle assigned to %s!"), *GetOwner()->GetName());
}

void UGrabberComponent::SetupInputComponent() {
	InputComponent = GetOwner()->FindComponentByClass<UInputComponent>();

	if (InputComponent) {
		InputComponent->BindAction("Grab", EInputEvent::IE_Pressed, this, &UGrabberComponent::Grab);
		InputComponent->BindAction("Grab", EInputEvent::IE_Released, this, &UGrabberComponent::Release);
	} else {
		UE_LOG(LogTemp, Error, TEXT("No InputComponent assigned to %s!"), *GetOwner()->GetName());
	}
}

FHitResult UGrabberComponent::GetFirstPhysicsBody() const {
	FHitResult Hit;

	FVector PlayerViewPointLocation;
	FRotator PlayerViewPointRotation;

	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OUT PlayerViewPointLocation,
		OUT PlayerViewPointRotation
	);

	FVector LineTraceEnd = GetLineTraceEnd();

	FCollisionQueryParams TraceParams = FCollisionQueryParams(
		FName(TEXT("")),
		false,
		GetOwner()
	);

	GetWorld()->LineTraceSingleByObjectType(
		OUT Hit,
		PlayerViewPointLocation,
		LineTraceEnd,
		FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
		TraceParams
	);

	return Hit;
}

FVector UGrabberComponent::GetLineTraceEnd() const {
	FVector PlayerViewPointLocation;
	FRotator PlayerViewPointRotation;

	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OUT PlayerViewPointLocation,
		OUT PlayerViewPointRotation
	);

	return PlayerViewPointLocation + PlayerViewPointRotation.Vector() * Reach;
}


