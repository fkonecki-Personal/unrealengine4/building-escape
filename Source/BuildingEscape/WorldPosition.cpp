// Fill out your copyright notice in the Description page of Project Settings.

#include <BuildingEscape/WorldPosition.h>
#include <GameFramework/Actor.h>

// Sets default values for this component's properties
UWorldPosition::UWorldPosition()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UWorldPosition::BeginPlay()
{
	Super::BeginPlay();

	FString objectName = GetOwner() -> GetName();
	FVector objectPosition = GetOwner() -> GetActorLocation();

	UE_LOG(LogTemp, Display, TEXT("Object %s is on position x: %f, y: %f, z: %f"), 
		*objectName, objectPosition.X, objectPosition.Y, objectPosition.Z);
	 
}


// Called every frame
void UWorldPosition::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

